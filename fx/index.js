'use strict';

const assert = require('assert');
const express = require('express');
const merge = require('merge');
const fs = require('fs');
const yaml = require('js-yaml');
const path = require('path');

const FxService = require('./src/fx-service');
const CoinbaseProvider = require('./src/providers/coinbase');
const OpenExchangeRatesProvider = require('./src/providers/open-exchange-rates');

module.exports = function (options, imports, register) {
  assert(imports.loggerFactory, 'Requires import `loggerFactory`');
  assert(imports.registerShutdownManager, 'Requires import `registerShutdownManager`');

  const fxRateExpiry = yaml.safeLoad(fs.readFileSync(path.join(__dirname, './data/fx-rate-expiry.yml'), 'utf8'));
  const publishSetFxRateTask = imports.registerWorkerTaskPublisher('set-fx-rate');
  imports.publishSetFxRateTask = publishSetFxRateTask;

  const fxService = new FxService(options, imports);
  const providersConfig = yaml.safeLoad(fs.readFileSync(path.join(__dirname, './data/providers.yml'), 'utf8'));

  const coinbaseOptions = merge(true, {
    provides: providersConfig['coinbase'],
  }, options.coinbase);
  fxService.provider(new CoinbaseProvider(coinbaseOptions, imports));

  const openExchangeRatesOptions = merge(true, {
    provides: providersConfig['open-exchange-rates'],
  }, options['open-exchange-rates']);
  fxService.provider(new OpenExchangeRatesProvider(openExchangeRatesOptions, imports));

  const fxRouter = express.Router();

  /**
   * @swagger
   * '/fx/{from}/{to}':
   *  get:
   *    summary: Get Exchange Rate
   *    description: Get the prevailing exchange rate multiplier between a currency pair
   *    operationId: getFxRate
   *    x-operation-name: getRate
   *    tags:
   *      - fx
   *    parameters:
   *      - name: from
   *        in: path
   *        description: Currency to get a rate for conversion from
   *        required: true
   *        schema:
   *          $ref: '#components/schemas/CurrencyCode'
   *      - name: to
   *        in: path
   *        description: Currency to get a rate for conversion to
   *        required: true
   *        schema:
   *          $ref: '#components/schemas/CurrencyCode'
   *    responses:
   *      '200':
   *        description: Fetched OK
   *        content:
   *          application/json:
   *            schema:
   *              type: object
   *              $ref: '#/components/schemas/FxRate'
   *      '405':
   *        $ref: '#/components/responses/NotSupportedError'
   */
  fxRouter.get('/fx/:from/:to', async function (req, res, next) {
    try {
      req.logger.debug('Getting rate...', req.params);
      req.params.from = req.params.from.toUpperCase();
      req.params.to = req.params.to.toUpperCase();
      const rate = await fxService.rate(req.params.from, req.params.to);
      req.logger.info('Got rate OK:', rate);
      res.send(rate);
    } catch (err) {
      next(err);
    }
  });

  /**
   * @swagger
   * '/fx/{from}/{to}/{amount}':
   *  get:
   *    summary: Convert Amount Using Exchange Rate
   *    description: Convert the given amount beteen the currency pair
   *    operationId: getFxConvertedAmount
   *    x-operation-name: convertAmount
   *    tags:
   *      - fx
   *    parameters:
   *      - name: from
   *        in: path
   *        description: Currency to get a rate for conversion from
   *        required: true
   *        schema:
   *          $ref: '#components/schemas/CurrencyCode'
   *      - name: to
   *        in: path
   *        description: Currency to get a rate for conversion to
   *        required: true
   *        schema:
   *          $ref: '#components/schemas/CurrencyCode'
   *      - name: amount
   *        in: path
   *        description: Amount to convert
   *        required: true
   *        schema:
   *          type: number
   *    responses:
   *      '200':
   *        description: Fetched OK
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/FxQuote'
   *      '405':
   *        $ref: '#/components/responses/NotSupportedError'
   */
  fxRouter.get('/fx/:from/:to/:amount', async function (req, res, next) {
    try {
      req.logger.debug('Getting quote...', req.params);
      req.params.from = req.params.from.toUpperCase();
      req.params.to = req.params.to.toUpperCase();
      const quote = await fxService.convert(req.params.amount, req.params.from, req.params.to);
      req.logger.info('Got quote OK.');
      // serialize into string with appropriate level of accuracy
      res.send({
        amount: quote.amount.toFixed(quote.amountCurrencyMeta.decimalDigits),
        amountCurrency: quote.amountCurrency,
        amountCurrencyMeta: quote.amountCurrencyMeta,
        value: quote.value.toFixed(quote.valueCurrencyMeta.decimalDigits),
        valueCurrency: quote.valueCurrency,
        valueCurrencyMeta: quote.valueCurrencyMeta,
      });
    } catch (err) {
      next(err);
    }
  });

  register(null, {
    fxRouter,
    fxService,
    publishSetFxRateTask,
    fxRateExpiry,
  });
};
