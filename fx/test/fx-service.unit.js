'use strict';

/* global describe, it */

const chai = require('chai');
const moment = require('moment');
const mocksConfig = require('../../../test-helpers/mocks');
const FxService = require('../src/fx-service');
const BigNumber = require('bignumber.js');
const imports = require('./../package.json');
const errors = require('common-errors');
const sinon = require('sinon');
const expect = chai.expect;
chai.use(require('chai-bignumber')());
chai.use(require('chai-as-promised'));
const mocks = mocksConfig();

const RemoteFxRateProvider = require('../src/providers/remote-fx-rate-provider');
class MockRemoteFxRateProvider extends RemoteFxRateProvider {
  constructor(name, provides, fn, canProvidefn) {
    super(name, provides);
    this.fn = fn;
    this.canProvidefn = canProvidefn;
  }
  async fetchRemoteRate() {
    return this.fn.apply(this, arguments);
  }
  canProvide() {
    return this.canProvidefn.apply(this, arguments);
  }
}
const momentValid = moment().add(2, 'hour');
describe('FxService', function () {
  describe('rate', function () {});

  describe('localRate', function () {
    // direct rate
    it('provides existing rate when non-expired local direct rate from:to exists', async function () {
      const mockFxRate = sinon.mock(mocks.FxRate);
      mockFxRate.expects('findOne').once().returns({
        from: 'USD',
        to: 'EUR',
        rate: new BigNumber(0.75),
        inverseRate: new BigNumber(1).dividedBy(0.75),
        expires: momentValid,
        get: function () {},
      });

      const service = new FxService(imports.plugin, mocks);
      const fxRate = await service.localRate('USD', 'EUR');

      mockFxRate.restore();
      expect(fxRate).to.deep.include({
        from: 'USD',
        to: 'EUR',
        expires: momentValid,
        rate: new BigNumber(0.75),
        inverseRate: new BigNumber(1).dividedBy(0.75),
      });
    });

    it('does not provide rate when no direct from:to exists', async function () {
      const service = new FxService(imports.plugin, mocks);
      const fxRate = await service.localRate('USD', 'EUR');

      expect(fxRate).to.deep.equal(null);
    });

    // inverse rate
    it('provides existing rate when non-expired local inverse rate to:from exists', async function () {
      const mockFxRate = sinon.mock(mocks.FxRate);
      mockFxRate.expects('findOne').returns({
        from: 'EUR',
        to: 'USD',
        rate: new BigNumber(0.75),
        inverseRate: new BigNumber(1).dividedBy(0.75),
        expires: momentValid,
        get: function () {},
      });

      const service = new FxService(imports.plugin, mocks);
      const fxRate = await service.localRate('USD', 'EUR');

      mockFxRate.restore();
      expect(fxRate).to.deep.include({
        from: 'EUR',
        to: 'USD',
        expires: momentValid,
        rate: new BigNumber(0.75),
        inverseRate: new BigNumber(1).dividedBy(0.75),
      });
    });

    it('does not provide rate when no inverse to:from exists', async function () {
      const service = new FxService(imports.plugin, mocks);
      const fxRate = await service.localRate('USD', 'EUR');

      expect(fxRate).to.deep.equal(null);
    });

    // indirect rate
    it('provides existing rate when non-expired local rate to:USD and USD:from exists', async function () {
      const mockFxRate = sinon.mock(mocks.FxRate);
      mockFxRate.expects('findOne').returns(null);
      // thb to usd -> usd to eur
      mockFxRate.expects('findOne').returns({
        from: 'THB',
        to: 'USD',
        rate: new BigNumber(0.75),
        inverseRate: new BigNumber(1).dividedBy(0.75),
        expires: momentValid,
        get: function () {},
        multiplier: function () {
          return 0.75;
        },
      });
      mockFxRate.expects('findOne').returns({
        from: 'USD',
        to: 'EUR',
        rate: new BigNumber(0.75),
        inverseRate: new BigNumber(1).dividedBy(0.75),
        expires: momentValid,
        get: function () {},
        multiplier: function () {
          return 0.75;
        },
      });
      mockFxRate.expects('build').returns({
        from: 'THB',
        to: 'EUR',
        rate: new BigNumber(0.75),
        get: function () {},
        multiplier: function () {
          return 0.75;
        },
      });

      const service = new FxService(imports.plugin, mocks);
      const fxRate = await service.localRate('THB', 'EUR');

      mockFxRate.restore();
      expect(fxRate).to.deep.include({
        from: 'THB',
        to: 'EUR',
        rate: new BigNumber(0.75),
      });
    });

    it('does not provide rate when no non-expired local rate to:USD and USD:from exists', async function () {
      const service = new FxService(imports.plugin, mocks);
      const fxRate = await service.localRate('THB', 'EUR');

      expect(fxRate).to.deep.equal(null);
    });
  });

  describe('remoteRate', function () {
    it('at least one provider for from:to local rate exists, succeeds', async function () {
      const mockFxRate = sinon.mock(mocks.FxRate);

      const canProvideRate = new MockRemoteFxRateProvider('mockRateSucceeds', [
        ['USD', 'EUR'],
      ], function () {
        return new BigNumber(0.5);
      }, function () {
        return true;
      });
      const cannotProvideRate = new MockRemoteFxRateProvider('secondAvailable', [
        ['USD', 'EUR'],
      ], function () {
        return new BigNumber(0.5);
      }, function () {
        return true;
      });

      mockFxRate.expects('build').returns({
        from: 'USD',
        to: 'EUR',
        rate: new BigNumber(0.5),
        inverseRate: new BigNumber(1).dividedBy(0.5),
        expires: momentValid,
        get: function () {},
        toJSON: function () {},
      });

      const service = new FxService(imports.plugin, mocks);
      service.provider(canProvideRate);
      service.provider(cannotProvideRate);
      const fxRate = await service.remoteRate('USD', 'EUR');

      mockFxRate.restore();
      expect(fxRate).to.deep.include({
        from: 'USD',
        to: 'EUR',
        expires: momentValid,
        rate: new BigNumber(0.5),
        inverseRate: new BigNumber(1).dividedBy(0.5),
      });
    });

    it('at least one provider for from:to local rate exists, but fails', async function () {
      const providerRateExists = new MockRemoteFxRateProvider('mockRateExists', [
        ['USD', 'EUR'],
      ], function () {
        return new BigNumber(0.5);
      }, function () {
        return false;
      });
      const providerRateDoesntExist = new MockRemoteFxRateProvider('mockRateDoesntExists', [
        ['THB', 'EUR'],
      ], function () {
        return new BigNumber(0.5);
      }, function () {
        return false;
      });

      const service = new FxService(imports.plugin, mocks);
      service.provider(providerRateExists);
      service.provider(providerRateDoesntExist);

      await expect(service.remoteRate('USD', 'EUR')).to.be.rejectedWith(errors.NotSupportedError, 'Could not find provider for pair USD:EUR');
    });

    it('more than one provider for from:to local rate exists, and one fails (but one succeeds)', async function () {
      const mockFxRate = sinon.mock(mocks.FxRate);

      const providerRateExists = new MockRemoteFxRateProvider('mockRateExists', [
        ['USD', 'EUR'],
      ], function () {
        return new BigNumber(0.5);
      }, function () {
        return true;
      });
      const providerFails = new MockRemoteFxRateProvider('mockProviderFails', [
        ['USD', 'EUR'],
      ], function () {
        return new BigNumber(0.5);
      }, function () {
        return false;
      });

      mockFxRate.expects('build').returns({
        from: 'USD',
        to: 'EUR',
        rate: new BigNumber(0.5),
        inverseRate: new BigNumber(1).dividedBy(0.5),
        expires: momentValid,
        get: function () {},
        toJSON: function () {},
      });

      const service = new FxService(imports.plugin, mocks);
      service.provider(providerRateExists);
      service.provider(providerFails);
      const fxRate = await service.remoteRate('USD', 'EUR');

      mockFxRate.restore();
      expect(fxRate).to.deep.include({
        from: 'USD',
        to: 'EUR',
        expires: momentValid,
        rate: new BigNumber(0.5),
        inverseRate: new BigNumber(1).dividedBy(0.5),
      });
    });

    it('no provider for from:to local does not exist', async function () {
      const service = new FxService(imports.plugin, mocks);
      await expect(service.remoteRate('USD', 'EUR')).to.be.rejectedWith(errors.NotSupportedError, 'Could not find provider for pair USD:EUR');
    });

    // to: from
    it('at least one provider for to:from local rate exists, succeeds', async function () {
      const mockFxRate = sinon.mock(mocks.FxRate);

      const canProvideRate = new MockRemoteFxRateProvider('mockRateSucceeds', [
        ['EUR', 'USD'],
      ], function () {
        return new BigNumber(0.5);
      }, function () {
        return true;
      });
      const cannotProvideRate = new MockRemoteFxRateProvider('secondAvailable', [
        ['EUR', 'USD'],
      ], function () {
        return new BigNumber(0.5);
      }, function () {
        return true;
      });

      mockFxRate.expects('build').returns({
        from: 'USD',
        to: 'EUR',
        rate: new BigNumber(0.5),
        inverseRate: new BigNumber(1).dividedBy(0.5),
        expires: momentValid,
        get: function () {},
        toJSON: function () {},
      });

      const service = new FxService(imports.plugin, mocks);
      service.provider(canProvideRate);
      service.provider(cannotProvideRate);
      const fxRate = await service.remoteRate('USD', 'EUR');

      mockFxRate.restore();
      expect(fxRate).to.deep.include({
        from: 'USD',
        to: 'EUR',
        expires: momentValid,
        rate: new BigNumber(0.5),
        inverseRate: new BigNumber(1).dividedBy(0.5),
      });
    });

    it('at least one provider for to:from local rate exists, but fails', async function () {
      const providerRateExists = new MockRemoteFxRateProvider('mockRateExists', [
        ['EUR', 'USD'],
      ], function () {
        return new BigNumber(0.5);
      }, function () {
        return false;
      });

      const service = new FxService(imports.plugin, mocks);
      service.provider(providerRateExists);
      await expect(service.remoteRate('USD', 'EUR')).to.be.rejectedWith(errors.NotSupportedError);
    });

    it('more than one provider for to:from local rate exists, and one fails (but one succeeds)', async function () {
      const mockFxRate = sinon.mock(mocks.FxRate);

      const providerRateExists = new MockRemoteFxRateProvider('mockRateExists', [
        ['EUR', 'USD'],
      ], function () {
        return new BigNumber(0.5);
      }, function () {
        return true;
      });
      const providerFails = new MockRemoteFxRateProvider('mockProviderFails', [
        ['EUR', 'USD'],
      ], function () {
        return new BigNumber(0.5);
      }, function () {
        return false;
      });

      mockFxRate.expects('build').returns({
        from: 'USD',
        to: 'EUR',
        rate: new BigNumber(0.5),
        inverseRate: new BigNumber(1).dividedBy(0.5),
        expires: momentValid,
        get: function () {},
        toJSON: function () {},
      });

      const service = new FxService(imports.plugin, mocks);
      service.provider(providerRateExists);
      service.provider(providerFails);
      const fxRate = await service.remoteRate('USD', 'EUR');

      mockFxRate.restore();
      expect(fxRate).to.deep.include({
        from: 'USD',
        to: 'EUR',
        expires: momentValid,
        rate: new BigNumber(0.5),
        inverseRate: new BigNumber(1).dividedBy(0.5),
      });
    });

    it('no provider for to:from local does not exist', async function () {
      const service = new FxService(imports.plugin, mocks);
      await expect(service.remoteRate('USD', 'EUR')).to.be.rejectedWith(errors.NotSupportedError);
    });
  });
});
