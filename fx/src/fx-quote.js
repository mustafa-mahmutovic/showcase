'use strict';

const assert = require('assert');
const merge = require('merge');

class FxQuote {
  constructor(params) {
    assert(params.amount, 'Requires `amount`');
    assert(params.amountCurrency, 'Requires `amountCurrency`');
    assert(params.amountCurrencyMeta, 'Requires `amountCurrencyMeta`');
    assert(params.value, 'Requires `value`');
    assert(params.valueCurrency, 'Requires `valueCurrency`');
    assert(params.valueCurrencyMeta, 'Requires `valueCurrencyMeta`');
    assert(params.expires, 'Requires `expires`');
    merge(this, params);
  }

  toString() {
    return JSON.stringify(this.toJSON(), null, 2);
  }

  toJSON() {
    return {
      amount: this.amount.toFixed(this.amountCurrencyMeta.decimalDigits),
      amountCurrency: this.amountCurrency,
      amountCurrencyMeta: this.amountCurrencyMeta,
      value: this.amount.toFixed(this.valueCurrencyMeta.decimalDigits),
      valueCurrency: this.valueCurrency,
      valueCurrencyMeta: this.valueCurrencyMeta,
      expires: this.expires.format(),
    };
  }
}

module.exports = FxQuote;
