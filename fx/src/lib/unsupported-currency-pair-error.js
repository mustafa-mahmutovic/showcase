'use strict';

const { helpers, ArgumentError } = require('common-errors');

const UnsupportedCurrencyPairError = helpers.generateClass('UnsupportedCurrencyPairError', {
  extends: ArgumentError,
  args: ['from', 'to', 'inner_error'],
  generateMessage: function () {
    return `Invalid or unsuppored currency pair: ${this.from}:${this.to}`;
  },
});

module.exports = UnsupportedCurrencyPairError;
