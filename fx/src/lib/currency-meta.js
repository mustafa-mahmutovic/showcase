'use strict';

const changeCase = require('change-case');
const errors = require('common-errors');
const currencies = require('./currencies');

function currencyMeta(currency) {
  const meta = currencies[currency];
  if (!meta) {
    throw new errors.NotSupportedError(`Currency ${currency} not found.`);
  }
  for (const key in meta) {
    const formattedKey = changeCase.camelCase(key);
    if (formattedKey !== key) {
      meta[formattedKey] = meta[key];
      delete meta[key];
    }
  }
  return meta;
}

module.exports = currencyMeta;
