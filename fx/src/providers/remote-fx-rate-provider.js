'use strict';

const errors = require('common-errors');
const moment = require('moment');
const UnsupportedCurrencyPairError = require('./../lib/unsupported-currency-pair-error');

class RemoteFxRateProvider {
  constructor(name, provides) {
    this.name = name;
    this.provides = provides;
  }

  /**
   * Abstract method that should be implemented to get the multiplier for the given currency pair.
   * This should not be called externally, but rather rate() should be used since it does checking.
   *
   * @param  {string} from    The origin currency code
   * @param  {string} to      The destination currency code
   * @param  {Moment} expires (optional) time this rate should be valid for
   * @return {BigNumber}      A multiplier that can be used to convert from:to as a BigNumber object
   * @throws UnsupposedCurrencyPairError
   */
  async fetchRemoteRate(from, to, expires) { // eslint-disable-line no-unused-vars
    throw new errors.NotSupportedError('Unimplemented (hint: extend this class and implement this method)');
  }

  /**
   * Checks if from:to currency pair is supported, if so,
   * it fetches the multiplier to convert from -> to currency
   *
   * @param  {string} from             The origin currency code
   * @param  {string} to               The destination currency code
   * @param  {Moment.Interval} expires (optional) time this rate should be valid for
   * @return {BigNumber}      A multiplier that can be used to convert from:to as a BigNumber object
   * @throws UnupposedCurrencyPairError
   */
  async rate(from, to, expires) {
    expires = expires || moment().add(30, 'minutes');
    if (!this.canProvide(from, to)) {
      throw new UnsupportedCurrencyPairError(from, to);
    }
    return this.fetchRemoteRate(from, to, expires);
  }

  /**
   * Check if the given pair is provided by this provider, automatically checking inverse.
   *
   * @param  {string} from Source currency code
   * @param  {string} to   Destination currency code
   * @return {Boolean}     Whether provided pair is available within this provider.
   */
  canProvide(from, to) {
    if (this._providesIndex[this._pairCode(from, to)]) {
      return true;
    }
    // try to find wildcard pair for from:*
    if (this._providesIndex[this._pairCode(from, '*')]) {
      return true;
    }
    // try to find wildcard pair for inverse rate
    if (this._providesIndex[this._pairCode(to, '*')]) {
      return true;
    }
    return false;
  }

  get provides() {
    return this._provides;
  }

  set provides(provides) {
    if (!Array.isArray(provides)) {
      throw new errors.ArgumentError('provides', 'Should be an array of pairs');
    }
    this._provides = provides;
    this._buildProvidesIndex();
  }

  get name() {
    return this._name;
  }

  set name(name) {
    this._name = name;
  }

  // internals

  _pairCode(from, to) {
    if ('*' === from) {
      throw new errors.NotSupportedError('Wildcard is only supported as the \'to\' currency.');
    }
    return `${from}:${to}`;
  }

  _buildProvidesIndex() {
    this._providesIndex = {};
    this._provides.forEach((pair) => {
      const [from, to] = pair;
      const pairCode = this._pairCode(from, to);
      this._providesIndex[pairCode] = true;

      // build inverse, but only for concrete code pair, not wildcard
      if ('*' === to) {
        return;
      }
      const inversePairCode = this._pairCode(to, from);
      this._providesIndex[inversePairCode] = true;
    });
  }
}

module.exports = RemoteFxRateProvider;
