'use strict';

const assert = require('assert');
const errors = require('common-errors');
const request = require('superagent');
const BigNumber = require('bignumber.js');

const RemoteFxRateProvider = require('./remote-fx-rate-provider');


class CoinbaseProvider extends RemoteFxRateProvider {
  constructor(options, imports) {
    assert(Array.isArray(options.provides), 'Requires option `provides`');
    assert(imports.loggerFactory, 'Requires import `loggerFactory`');

    super('coinbase', options.provides);
    this.logger = imports.loggerFactory.create('coinbaseFxProvider');
    this.API_BASE_URL = 'https://api.coinbase.com/v2/exchange-rates';
  }

  async fetchRemoteRate(from, to, expires) {
    this.logger.debug(`Fetching remote rate for pair ${from}:${to} expiring on/after ${expires}`);
    const url = `${this.API_BASE_URL}?currency=${from}`;
    const res = await request.get(url).set('Accept', 'application/json');
    if (!res.body || !res.body.data || !res.body.data.rates) {
      throw new errors.ArgumentNullError('Service unavailable: expected property data.rates');
    }
    if (!res.body.data.rates[to]) {
      throw new errors.NotSupportedError(`Currency code ${to} not found in response.`);
    }
    const multiplier = res.body.data.rates[to];
    this.logger.info(`Fetched remote rate for pair ${from}:${to} expiring on/after ${expires} OK: ${multiplier}`);
    return new BigNumber(multiplier);
  }
}

module.exports = CoinbaseProvider;
