'use strict';

const assert = require('assert');
const cc = require('simple-open-exchange-rates');
const BigNumber = require('bignumber.js');

const RemoteFxRateProvider = require('./remote-fx-rate-provider');

class OpenExchangeRatesProvider extends RemoteFxRateProvider {
  constructor(options, imports) {
    assert(options.apiKey, 'Requires option `apiKey`');
    assert(Array.isArray(options.provides), 'Requires option `provides`');
    assert(imports.loggerFactory, 'Requires import `loggerFactory`');
    assert(imports.registerShutdownManager, 'Requires import `registerShutdownManager`');

    super('open-exchange-rates', options.provides);

    this.logger = imports.loggerFactory.create('openExchangeRatesFxProvider');
    this.service = cc({
      CLIENTKEY: options.apiKey,
    });

    // the service has an internal daemon, so needs to be manually stopped
    // otherwise scripts won't terminate normally
    imports.registerShutdownManager('simple-open-exchange-rates', () => this.service.shutdown());
  }

  async fetchRemoteRate(from, to, expires) {
    this.logger.debug(`Fetching remote rate for pair ${from}:${to} expiring on/after ${expires}`);
    const multiplier = await this.service.rates(from, to);
    this.logger.info(`Fetched remote rate for pair ${from}:${to} expiring on/after ${expires} OK: ${multiplier}`);
    return new BigNumber(multiplier);
  }
}

module.exports = OpenExchangeRatesProvider;
