'use strict';

const assert = require('assert');
const errors = require('common-errors');
const Promise = require('bluebird');
const BigNumber = require('bignumber.js');
const moment = require('moment');
const path = require('path');
const fs = require('fs');
const yaml = require('js-yaml');

const RemoteFxRateProvider = require('./providers/remote-fx-rate-provider');

const getCurrencyMeta = require('./lib/currency-meta');
const FxQuote = require('./fx-quote');
const Op = require('sequelize').Op;

class FxService {
  constructor(options, imports) {
    assert(imports.FxRate, 'Requires import `FxRate`');
    assert(imports.loggerFactory, 'Requires import `loggerFactory`');
    assert(imports.publishSetFxRateTask, 'Requires import `publishSetFxRateTask`');

    this.enableLocalRates = options.enableLocalRates;
    this.enableRemoteRates = options.enableRemoteRates;

    if (!this.enableLocalRates && !this.enableRemoteRates) {
      throw new Error('Neither enableLocalRates=true nor enableRemoteRates=true; at least one must be enabled.');
    }

    this.FxRate = imports.FxRate;
    this.logger = imports.loggerFactory.create('fxService', 'fx-service');
    this.expiry = yaml.safeLoad(fs.readFileSync(path.join(__dirname, '/../data/fx-rate-expiry.yml'), 'utf8'));
    this.publishSetFxRateTask = imports.publishSetFxRateTask;
    this.providers = {};
  }

  provider(service) {
    if ('string' === typeof service) {
      return this.providers[service];
    }
    if (service instanceof RemoteFxRateProvider) {
      this.providers[service.name] = service;
      return this.providers[service];
    }
    throw new errors.NotSupportedError('provider must inherit from RemoteFxRateProvider');
  }

  async rate(from, to, expiry) {
    expiry = this.assertDurationAndGetDefault(from, to, expiry);
    let rate;
    if (this.enableLocalRates || !this.enableRemoteRates) {
      rate = await this.localRate(from, to, expiry);
      if (rate) {
        return rate;
      }
    }
    return await this.remoteRate(from, to, expiry);
  }

  async convert(amount, from, to, expiry) {
    expiry = this.assertDurationAndGetDefault(from, to, expiry);
    this.logger.debug(`Converting amount ${amount} for pair ${from}:${to} valid until ${expiry}...`);
    if (!(amount instanceof BigNumber)) {
      amount = new BigNumber(amount);
    }
    const currencyFromMeta = getCurrencyMeta(from);
    const currencyToMeta = getCurrencyMeta(to);
    const fxRate = await this.rate(from, to, expiry);
    const value = fxRate.convert(amount, from);
    const expires = moment().add(expiry);
    const result = new FxQuote({
      // for serialization, use:
      // amount: amount.toString(quote.from.decimalDigits),
      amount: amount,
      amountCurrency: from,
      amountCurrencyMeta: currencyFromMeta,
      // for serialization, use:
      // value: converted.toString(quote.to.decimalDigits),
      value,
      valueCurrency: to,
      valueCurrencyMeta: currencyToMeta,
      expires,
    });
    this.logger.info(`Converted amount ${amount} for pair ${from}:${to} valid until ${expires} OK:`);
    this.logger.debug(result.toJSON());
    return result;
  }

  async localRate(from, to, expiry) {
    expiry = this.assertDurationAndGetDefault(from, to, expiry);
    this.logger.debug(`Getting local rate from DB for pair ${from}:${to} expiring after ${expiry}...`);

    this.logger.debug(`Finding direct rate for pair ${from}:${to} expiring after ${expiry}...`);
    const expires = moment().add(expiry);
    const directRate = await this.FxRate.findOne({
      where: {
        [Op.or]: [{
          from: from,
          to: to,
          expires: {
            [Op.gt]: expires,
          },
        }, {
          from: to,
          to: from,
          expires: {
            [Op.gt]: expires,
          },
        }],
      },
    });
    if (directRate) {
      this.logger.info(`Found direct rate for pair ${from}:${to} expiring after ${expiry} OK:`, directRate.get({
        plain: true,
      }));
      return directRate;
    }
    this.logger.info(`No direct rate for pair ${from}:${to} expiring after ${expiry}.`);

    this.logger.debug(`Finding intermediate rate via ${from}:USD:${to} expiring after ${expiry}...`);
    // find intermediate pair, if it exists
    const [srcToUsd, usdToDest] = await Promise.all([
      this.FxRate.findOne({
        where: {
          [Op.or]: [{
            from: from,
            to: 'USD',
            expires: {
              [Op.gt]: expires,
            },
          }, {
            from: 'USD',
            to: from,
            expires: {
              [Op.gt]: expires,
            },
          }],
        },
      }),
      this.FxRate.findOne({
        where: {
          [Op.or]: [{
            from: to,
            to: 'USD',
            expires: {
              [Op.gt]: expires,
            },
          }, {
            from: 'USD',
            to: to,
            expires: {
              [Op.gt]: expires,
            },
          }],
        },
      }),
    ]);

    if (!srcToUsd) {
      this.logger.info(`No rate for ${from}:USD expiring after ${expiry}.`);
      return null;
    }
    if (!usdToDest) {
      this.logger.info(`No rate for USD:${to} expiring after ${expiry}.`);
      return null;
    }
    this.logger.info(`Found new rate for ${from}:USD and USD:${to} expiring after ${expiry}`);

    this.logger.debug(`Building rate from ${from}:USD and USD:${to} expiring after ${expiry}...`);
    const srcMultiplier = new BigNumber(srcToUsd.multiplier(from));
    const targetMultiplier = new BigNumber(usdToDest.multiplier(to));
    const multiplier = srcMultiplier.times(targetMultiplier);
    const result = this.FxRate.build({
      from,
      to,
      rate: multiplier,
    });
    this.logger.info(`Built rate from ${from}:USD and USD:${to} expiring after ${expiry} OK:`, result.get({
      plain: true,
    }));
    return result;
  }

  async remoteRate(from, to, expiry) {
    expiry = this.assertDurationAndGetDefault(from, to, expiry);
    this.logger.debug(`Fetching remote rate for pair ${from}:${to} expiring after ${expiry}...`);

    const providers = this.getProvidersForPair(from, to);
    if (!providers.length) {
      throw new errors.NotSupportedError(`Could not find provider for pair ${from}:${to}`);
    }

    // accept whichever one succeeds first
    const rate = await Promise.any(providers.map(async provider => provider.rate(from, to, expiry)));
    const inverseRate = (new BigNumber(1)).dividedBy(rate);
    const expires = moment().add(expiry);

    const fxRate = this.FxRate.build({
      from,
      to,
      rate,
      inverseRate,
      expires,
    });
    await this.publishSetFxRateTask(fxRate.get({ plain: true }));
    this.logger.info(`Fetched remote rate for pair ${from}:${to} expiring after ${expires.format()} OK:`, fxRate.toJSON());
    return fxRate;
  }

  getProvidersForPair(from, to) {
    const names = Object.keys(this.providers);
    const supported = [];
    names.forEach((name) => {
      const provider = this.providers[name];
      if (provider.canProvide(from, to)) {
        supported.push(provider);
      }
    });
    return supported;
  }

  currencyToFixed(number, currency) {
    const meta = getCurrencyMeta(currency);
    if (!BigNumber.isBigNumber(number)) {
      number = new BigNumber(number);
    }
    if (!meta.decimalDigits && 0 !== meta.decimalDigits) {
      throw new errors.NotSupportedError(`Currency ${currency} has no meta info for decimalDigits`);
    }
    return number.toFixed(meta.decimalDigits);
  }

  getDefaultExpiry() {
    return moment.duration(this.expiry.default.duration, this.expiry.default.unit);
  }

  getExpiryInterval(from, to) {
    let unit;
    let duration;
    if (this.expiry[from] && this.expiry[from][to]) {
      unit = this.expiry[from][to].unit;
      duration = this.expiry[from][to].duration;
    } else if (this.expiry[to] && this.expiry[to][from]) {
      unit = this.expiry[to][from].unit;
      duration = this.expiry[to][from].duration;
    } else {
      unit = this.expiry.default.unit;
      duration = this.expiry.default.duration;
    }
    return moment.duration(duration, unit);
  }

  assertDurationAndGetDefault(from, to, expiry) {
    if (expiry) {
      if (!moment.isDuration(expiry)) {
        throw new errors.ArgumentError('Expiry must be a valid Moment.duration object');
      }
      return expiry;
    }
    return this.getExpiryInterval(from, to);
  }
}

module.exports = FxService;
