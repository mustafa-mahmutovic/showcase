'use strict';

const assert = require('assert');
const Promise = require('bluebird');
const schedule = require('node-schedule');
// const Op = require('sequelize').Op;

module.exports = async function (options, imports, register) {
  assert(imports.fxService, 'Requires import `fxService`');
  assert(imports.fxRateExpiry, 'Requires import `fxRateExpiry`');
  assert(imports.loggerFactory, 'Requires import `loggerFactory`');
  assert(imports.sequelize, 'Requires import `sequelize`');
  assert(imports.registerModelEvent, 'Requires import `registerModelEvent`');
  assert(imports.registerWorkerTaskConsumer, 'Requires import `registerWorkerTaskConsumer`');
  assert(imports.registerWorkerTaskPublisher, 'Requires import `registerWorkerTaskPublisher`');

  try {
    imports.logger = imports.loggerFactory.create('fxWorker', 'fx-worker');

    const startFxWorker = async () => {
      // we use paranoid: true mode, so only create is relevant
      // since no update, and delete methods will be invoked
      // the only DB event relevant will always be creation of a new instance
      imports.registerModelEvent(imports.FxRate, 'afterCreate');

      imports.logger.info('Registering fx rate worker tasks...');
      imports.updateFxRate = imports.registerWorkerTaskPublisher('update-fx-rate');
      imports.registerWorkerTaskConsumer('update-fx-rate', require('./src/workers/update-fx-rate')(options, imports));
      imports.registerWorkerTaskConsumer('set-fx-rate', require('./src/workers/set-fx-rate')(options, imports));
      const dataKeys = Object.keys(imports.fxRateExpiry);
      const pairs = [];
      dataKeys
        .forEach((key) => {
          if (key === 'default') {
            return;
          }
          const currentKeys = Object.keys(imports.fxRateExpiry[key]);
          currentKeys.forEach((currentKey) => {
            pairs.push({ from: key, to: currentKey });
          });
        });

      await Promise.each(pairs, async (pair) => {
        const unit = imports.fxRateExpiry[pair.from][pair.to].unit;
        const duration = imports.fxRateExpiry[pair.from][pair.to].duration;
        const rule = new schedule.RecurrenceRule();

        rule[unit] = duration;
        schedule.scheduleJob(rule, function () {
          imports.logger.info(`Triggering scheduled refresh of FxRate: ${pair.from}:${pair.to}`);
          imports.updateFxRate(pair);
        });
        imports.updateFxRate(pair);
      });
    };

    register(null, { startFxWorker });
  } catch (err) {
    register(err);
  }
};
