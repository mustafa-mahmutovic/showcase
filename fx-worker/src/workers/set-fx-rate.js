'use strict';

const Op = require('sequelize')
  .Op;

module.exports = (options, imports) => {
  const logger = imports.logger.createChild('set-fx-rate');

  return async (data, ack, nack, msg) => {
    /* eslint no-unused-vars: 0 */
    const query = {
      [Op.or]: [{
        from: data.from,
        to: data.to,
      }, {
        from: data.to,
        to: data.from,
      }],
    };
    logger.debug(`Finding existing rates for currency pair:${data.from}:${data.to}...`);
    await imports.FxRate.destroy({ where: query });
    logger.info(`Found existing rate for currency pair:${data.from}:${data.to} OK.`);
    logger.debug(`Creating new FxRate for currency pair ${data.from}:${data.to}...`);
    const fxRate = await imports.FxRate.create(data);
    logger.info(`Created new FxRate for currency pair ${data.from}:${data.to} OK.`);
    return fxRate;
  };
};
