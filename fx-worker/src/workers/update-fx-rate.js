'use strict';


module.exports = (options, imports) => {
  const logger = imports.logger.createChild('update-fx-rate');
  return async (data, ack, nack, msg) => {
    /* eslint no-unused-vars: 0 */
    logger.debug(`Fetching remote FxRate for pair:${data.from}:${data.to}...`);
    const rate = await imports.fxService.remoteRate(data.from, data.to);
    logger.info(`Updated ${data.from} to ${data.to} rate is: ${rate.rate}`);
  };
};
